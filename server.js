const express = require('express');
const bodyParser = require('body-parser');
const https = require('https');
const fs = require('fs');
const swaggerUi = require('swagger-ui-express');
// const YAML = require('yamljs');

// const swaggerDocument = YAML.load('./swagger.yaml');
const swaggerDocument = require('./swagger2.json');

const app = express();

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept',
  );
  next();
});
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

const options = {
  key: fs.readFileSync('./ssl/key.pem'),
  cert: fs.readFileSync('./ssl/cert.pem'),
  passphrase: 'Mrt2487!',
};

const port = process.env.PORT || 3000;

https.createServer(options, app).listen(port, () => {
  console.log(`Server is up on port ${port}...`);
});
